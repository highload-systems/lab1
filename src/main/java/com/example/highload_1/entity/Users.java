package com.example.highload_1.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

    private String surname;

    private String phone;

    @ManyToOne
    private Team team;

    private String username;

    private String password;

    public Users(String name, String surname, String phone, Team team, String username, String password){
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.team = team;
        this.username = username;
        this.password = password;
    }
}
