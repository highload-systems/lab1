package com.example.highload_1.entity;

import com.example.highload_1.enums.MessageStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    private Users sender;

    @ManyToOne
    private Users receiver;

    private MessageStatus status;

    private LocalDateTime sendDate;

    private LocalDateTime readDate;

    @ManyToOne
    private Chat chat;

    private String textMessage;

}
