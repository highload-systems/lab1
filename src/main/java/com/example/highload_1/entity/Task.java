package com.example.highload_1.entity;

import com.example.highload_1.enums.Priority;
import com.example.highload_1.enums.TaskDifficulty;
import com.example.highload_1.enums.TaskType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String title;

    @ManyToOne
    private Project project;

    @ManyToOne
    private Users performer;

    private String description;

    private LocalDateTime creationDate;

    private LocalDateTime completeDate;

    private LocalDateTime deadline;

    private Priority priority;

    private TaskType taskType;

    private TaskDifficulty difficulty;

}
