package com.example.highload_1.entity;

import com.example.highload_1.enums.ChatType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private ChatType type;

    private String title;

    private String description;

    private String imagePath;

}
