package com.example.highload_1.entity;

import com.example.highload_1.enums.ChatRoleType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import static jakarta.persistence.FetchType.LAZY;

@Entity
@Getter
@Setter
public class ChatRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = LAZY)
    private Chat chat;

    @ManyToOne(fetch = LAZY)
    private Users user;

    private ChatRoleType chatRoleType;
}
