package com.example.highload_1.entity;

import com.example.highload_1.enums.ProjectRoleType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class ProjectRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    private Users user;

    @ManyToOne
    private Project project;

    private ProjectRoleType projectRoleType;
}
