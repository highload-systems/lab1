package com.example.highload_1.services;

import com.example.highload_1.DTO.UserRegisterRequest;
import com.example.highload_1.entity.Team;
import com.example.highload_1.entity.Users;
import com.example.highload_1.exceptions.UserAlreadyExistsException;
import com.example.highload_1.repositories.TeamRepository;
import com.example.highload_1.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UsersRepository userRepository;

    @Autowired
    private TeamRepository teamRepository;

    public void register(UserRegisterRequest request) throws UserAlreadyExistsException{
        if (userRepository.existsByUsername(request.getUsername())){
            throw new UserAlreadyExistsException(request.getUsername());
        }
        Team team = teamRepository.getTeamById(request.getTeam_id());
        Users user = new Users(request.getName(), request.getSurname(), request.getPhoneNumber(), team, request.getUsername(), request.getPassword());
        userRepository.save(user);
    }
}
