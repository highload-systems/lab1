package com.example.highload_1.services;

import com.example.highload_1.DTO.CreateTeamRequest;
import com.example.highload_1.entity.Team;
import com.example.highload_1.exceptions.TeamAlreadyExistsException;
import com.example.highload_1.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    public void createTeam(CreateTeamRequest request) throws TeamAlreadyExistsException {
        if (teamRepository.existsByName(request.getName())) {
            throw new TeamAlreadyExistsException(request.getName());
        }
        Team team = new Team(request.getName(), request.getDescription());
        teamRepository.save(team);
    }
}
