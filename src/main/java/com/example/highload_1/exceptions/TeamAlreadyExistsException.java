package com.example.highload_1.exceptions;

public class TeamAlreadyExistsException extends Exception{
    public TeamAlreadyExistsException(String name){
        super("Team with name " + name + " is already exists.");
    }
}
