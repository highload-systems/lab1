package com.example.highload_1.exceptions;

public class UserAlreadyExistsException extends Exception{
    public UserAlreadyExistsException(String username){
        super("User with name " + username + " is already exists.");
    }
}
