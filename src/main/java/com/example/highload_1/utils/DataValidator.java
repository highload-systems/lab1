package com.example.highload_1.utils;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Slf4j
@Component
@NoArgsConstructor
public class DataValidator {

    private final Pattern phoneNumberPattern = Pattern.compile("((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}");

    public boolean validateUser(String name, String surname, String phoneNumber,
                                Long team_id, String username, String password) {
        if (name == null || surname == null || phoneNumber == null || team_id == null || username == null || password == null) {
            return false;
        }

        if (!phoneNumberPattern.matcher(phoneNumber).find()){
            return false;
        }

        if (username.length() <= 3) { return false; }
        if (password.length() < 15) { return false; }

        return true;
    }

    public boolean validateTeam(String name, String description){
        if (name == null || description == null) {
            return false;
        }
        if(name.length() <= 2){
            return false;
        }
        return true;
    }

}
