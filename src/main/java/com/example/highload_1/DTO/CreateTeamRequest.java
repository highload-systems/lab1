package com.example.highload_1.DTO;

import lombok.Data;

@Data
public class CreateTeamRequest {
    private String name;
    private String description;
}
