package com.example.highload_1.DTO;

import lombok.Data;
@Data
public class UserRegisterRequest {
    private String name;
    private String surname;
    private String phoneNumber;
    private Long team_id;
    private String username;
    private String password;

}
