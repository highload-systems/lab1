package com.example.highload_1.enums;

import java.util.Set;

public interface Role {
    boolean includes(Role role);

    static Set<Role> roots() {
        return Set.of(ProjectRoleType.TEAM_LEADER, ChatRoleType.ADMIN);
    }

}
