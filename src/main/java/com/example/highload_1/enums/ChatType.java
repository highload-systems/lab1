package com.example.highload_1.enums;

public enum ChatType {
    PRIVATE, GROUP, CHANNEL
}
