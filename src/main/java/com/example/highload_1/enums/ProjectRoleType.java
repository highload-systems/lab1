package com.example.highload_1.enums;

import java.util.HashSet;
import java.util.Set;

public enum ProjectRoleType implements Role {
    VIEWER, DEVELOPER, TEAM_LEADER;

    private final Set<Role> children = new HashSet<>();

    static {
        DEVELOPER.children.add(VIEWER);
        TEAM_LEADER.children.add(DEVELOPER);
    }

    @Override
    public boolean includes(Role role) {
        return this.equals(role) || children.stream().anyMatch(r -> r.includes(role));
    }

}
