package com.example.highload_1.enums;

import java.util.HashSet;
import java.util.Set;

public enum ChatRoleType implements Role {
    READER, PARTICIPANT, ADMIN;

    private final Set<Role> children = new HashSet<>();

    static {
        PARTICIPANT.children.add(READER);
        ADMIN.children.add(PARTICIPANT);
    }

    @Override
    public boolean includes(Role role) {
        return this.equals(role) || children.stream().anyMatch(r -> r.includes(role));
    }
}
