package com.example.highload_1.enums;

public enum TaskType {
    MEETING, DEVELOPMENT, BUG_FIX
}
