package com.example.highload_1.enums;

public enum TaskDifficulty {
    EASY, MEDIUM, HARD
}
