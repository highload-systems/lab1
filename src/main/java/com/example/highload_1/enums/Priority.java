package com.example.highload_1.enums;

public enum Priority {
    LOW, MEDIUM, HIGH, VERY_HIGH
}
