package com.example.highload_1.enums;

public enum MessageStatus {
    SENT, DELIVERED, READ, DELETED
}
