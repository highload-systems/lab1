package com.example.highload_1.controllers;

import com.example.highload_1.DTO.CreateTeamRequest;
import com.example.highload_1.exceptions.TeamAlreadyExistsException;
import com.example.highload_1.services.TeamService;
import com.example.highload_1.utils.DataValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/team")
public class TeamController {

    @Autowired
    private DataValidator validator;

    @Autowired
    private TeamService teamService;

    @PostMapping("/create")
    public ResponseEntity<?> register(@RequestBody CreateTeamRequest request){
        if (validator.validateTeam(request.getName(), request.getDescription())){
            try {
                teamService.createTeam(request);
                return ResponseEntity.status(HttpStatus.OK).build();
            } catch (TeamAlreadyExistsException existsException) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(existsException.getMessage());
            }
        }
        return ResponseEntity.badRequest().body("Invalid data");
    }
}
