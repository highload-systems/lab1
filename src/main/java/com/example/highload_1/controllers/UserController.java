package com.example.highload_1.controllers;

import com.example.highload_1.DTO.UserRegisterRequest;
import com.example.highload_1.exceptions.UserAlreadyExistsException;
import com.example.highload_1.services.UserService;
import com.example.highload_1.utils.DataValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private DataValidator validator;

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserRegisterRequest request){
        if (validator.validateUser(request.getName(), request.getSurname(),
                request.getPhoneNumber(), request.getTeam_id(), request.getUsername(), request.getPassword())){
            try {
                userService.register(request);
                return ResponseEntity.status(HttpStatus.OK).build();
            } catch (UserAlreadyExistsException existsException) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(existsException.getMessage());
            }
        }
        return ResponseEntity.badRequest().body("Invalid data");
    }

}